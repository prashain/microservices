package com.pras.codelabs.microservices.profile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;

@SpringBootApplication
@EnableResourceServer
public class ProfileApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProfileApiApplication.class, args);
	}
	
//    @Bean
//    @Primary
//    public ResourceServerTokenServices tokenService() {
//        RemoteTokenServices tokenServices = new RemoteTokenServices();
//        tokenServices.setClientId("web");
//        tokenServices.setClientSecret("webpass");
//        tokenServices.setCheckTokenEndpointUrl("http://localhost:8181/oauth/check_token");
//        return tokenServices;
//    }

}
