package com.pras.codelabs.microservices.profile;

public class Profile {

	Long id;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	String profileName;
	String profileDescription;



	public Profile(Long id, String profileName, String profileDescription) {
		super();
		this.id = id;
		this.profileName = profileName;
		this.profileDescription = profileDescription;
	}

	public Profile() {
	}

	public String getProfileName() {
		return profileName;
	}

	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}

	public String getProfileDescription() {
		return profileDescription;
	}

	public void setProfileDescription(String profileDescription) {
		this.profileDescription = profileDescription;
	}

	@Override
	public String toString() {
		return "Profile [profileName=" + profileName + ", profileDescription=" + profileDescription + "]";
	}
	
	
}
