package com.pras.codelabs.microservices.profile;

import java.util.List;


public interface ProfileService {

	List<Profile> findAll();

	Profile findProfileById(Long id);
	
	

}
