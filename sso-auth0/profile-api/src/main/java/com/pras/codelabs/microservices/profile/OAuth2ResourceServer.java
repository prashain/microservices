package com.pras.codelabs.microservices.profile;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;

@Configuration

public class OAuth2ResourceServer extends ResourceServerConfigurerAdapter 
{
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
            .antMatchers("/api/**")
            .authenticated()
            .antMatchers("/").permitAll();
    }
//    @Bean
//    @Primary
//    public ResourceServerTokenServices tokenService() {
//        RemoteTokenServices tokenServices = new RemoteTokenServices();
//        tokenServices.setClientId("web");
//        tokenServices.setClientSecret("webpass");
//        tokenServices.setCheckTokenEndpointUrl("http://localhost:8181/oauth/check_token");
//        return tokenServices;
//    }
}