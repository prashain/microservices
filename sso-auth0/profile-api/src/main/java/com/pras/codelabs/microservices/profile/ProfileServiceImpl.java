package com.pras.codelabs.microservices.profile;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class ProfileServiceImpl  implements ProfileService{

	@Override
	public List<Profile> findAll() {
		return Arrays.asList(
				new Profile(1l,"name","foo"),
				new Profile(2l,"adas","ababa")
				);
				
	}

	@Override
	public Profile findProfileById(Long id) {
		return new Profile(1l,"name","foo");
	}
	
	

}
