package com.pras.codelabs.microservices.profile;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ProfileController {
	
	private static final Logger logger = LoggerFactory.getLogger(ProfileController.class);

	
	@Autowired
	ProfileService profileService;



	@GetMapping(value="/profiles")
	public List<Profile> list() {
		return profileService.findAll();
	}
	
	@GetMapping(value = "/profiles/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin(maxAge = 6000)
	@ResponseBody
	public ResponseEntity<Profile> getMethodName(@PathVariable("id") Long id) {
		Authentication loggedInUser=  SecurityContextHolder.getContext().getAuthentication();
	        String email = loggedInUser.getName() + "@howtodoinjava.com";
	        logger.info("the password is {}",loggedInUser.getCredentials());
	        logger.info("the email is {}",email);
	        Profile profile = new Profile();
	        profile.setProfileName(email);
	        profile.setId(5l);
	        profile.setProfileDescription("someRandomDes");
		return ResponseEntity.ok(profile);
	}
 
}
